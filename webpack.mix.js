let mix = require('laravel-mix');

mix.js('resources/assets/js/cms.js', 'public/js')
    .sass('resources/assets/sass/cms.scss', 'public/css')
    .then(() => {
        var exec = require('child_process').exec;

        exec('php ./../artisan vendor:publish --tag=air --force', function (error, stdOut, stdErr) {
        });
    });
