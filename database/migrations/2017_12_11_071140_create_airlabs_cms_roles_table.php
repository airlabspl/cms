<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlabsCmsRolesTable extends Migration
{
    public function up()
    {
        Schema::create('airlabs_cms_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('airlabs_cms_permission_role', function (Blueprint $table) {
            $table->string('permission');
            $table->unsignedInteger('role_id');

            $table->foreign('role_id')
                ->references('id')
                ->on('airlabs_cms_roles')
                ->onDelete('cascade');

            $table->primary([ 'permission', 'role_id' ]);
        });
    }

    public function down()
    {
        Schema::dropIfExists('airlabs_cms_roles');
        Schema::dropIfExists('airlabs_cms_permission_role');
    }
}
