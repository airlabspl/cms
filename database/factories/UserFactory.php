<?php

use Faker\Generator as Faker;

$factory->define(\Airlabs\Cms\User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'email' => $faker->unique()->email,
        'password' => bcrypt($faker->password),
        'is_admin' => false
    ];
});

$factory->state(\Airlabs\Cms\User::class, 'admin', function (Faker $faker) {
    return [
        'is_admin' => true
    ];
});
