<?php

use Faker\Generator as Faker;

$factory->define(\Airlabs\Cms\Role::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
