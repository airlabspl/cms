<?php

/** Auth */
Route::get('airlabs/cms/login', 'SessionsController@create')
    ->name('airlabs.cms.sessions.create');
Route::post('airlabs/cms/login', 'SessionsController@store')
    ->name('airlabs.cms.sessions.store');

Route::group([ 'middleware' => 'airlabs.cms.auth:air' ], function() {
    /** Dashboard */
    Route::get(air()->dashboardUrl(), 'DashboardController@index');

    /** Auth */
    Route::get('airlabs/cms/logout', 'SessionsController@destroy')
        ->name('airlabs.cms.sessions.destroy');
});