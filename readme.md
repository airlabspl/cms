# AIR Labs CMS

AIR Labs CMS is a content management system aimed towards Laravel developers. 
It provides clean authorization of CMS administrators and users but does not imply
any rules of creating website-specific code.

### Installation

Require package. Service Provider will be registered automatically.

```
composer require airlabs/cms
```


Publish assets to `/public` directory:

```
php artisan vendor:publish --tag=air --force
```

Migrate tables

```
php artisan migrate
```


### Creating admins

There are no admins by default. To create one hit the command in the console:

```
php artisan create:admin
```

and follow the prompts.


### Usage

By default you can access admin panel at `/admin`. It is fully customizable in configuration.


### Creating Plugins

Create your plugin class and implement `Airlabs\Cms\Plugin` interface. Then register it
withing any service provider by issuing code:

```
air()->plugins()->register(new MyPlugin());
```

It should now be visible in the dashboard. The default path of your plugin will use plugin's `name()` method.
E.g. if you create a plugin with a name **acme/test** you should create a page pointing to **http://website.dev/acme/test**.

### Creating admin pages

This package comes with two layout for your views. Just create a blade view and extend `air::layouts.plugin`. The content goes to `plugin` section.


### Admin protected routes

If you create pages for admin dashboard use the `air` and `airlabs.cms.auth:air` middlewares, e.g.:


```
Route::group([ 'middleware' => [ 'air', 'airlabs.cms.auth:air' ] ], function () {
    Route::get('acme/test', 'AcmeTestController@index');
});
```
