<?php

namespace Airlabs\Cms;

use Airlabs\Cms\Roles\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles, Notifiable;

    protected $table = 'airlabs_cms_users';

    protected $fillable = [
        'name', 'email', 'password', 'role_id',
        'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'is_admin' => 'boolean'
    ];

    public function admin()
    {
        return true === $this->is_admin;
    }

    public function canEdit(User $user)
    {
        if($this->id === $user->id) return false;
        if($user->admin()) return false;

        return true;
    }
}
