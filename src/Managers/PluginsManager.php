<?php

namespace Airlabs\Cms\Managers;

use Airlabs\Cms\Plugin;
use Airlabs\Cms\Plugins\SettingsPlugin;
use Illuminate\Support\Collection;

class PluginsManager
{
    protected $plugins = [];

    public function register(Plugin $plugin)
    {
        $this->plugins[] = $plugin;
    }

    public function all(): Collection
    {
        $cmsPlugins = [
            //
        ];

        return collect(
            array_merge($this->plugins, $cmsPlugins)
        )->filter(function ($plugin) {
            if ( ! method_exists($plugin, 'permission')) return true;

            return auth('air')->user()->can($plugin->permission());
        });
    }
}
