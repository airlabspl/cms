<?php

namespace Airlabs\Cms\Managers;

use Illuminate\Support\Facades\Gate;

class PermissionsManager
{
    public function register()
    {
        if ( ! auth('air')->check()) return;

        $permissions = auth('air')->user()->role->permissions();

        Gate::before(function ($user, $ability) {
            if ($user->admin()) return true;
        });

        foreach ($permissions as $permission) {
            Gate::define($permission, function () {
                return true;
            });
        }
    }
}
