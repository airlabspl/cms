<?php

namespace Airlabs\Cms\Roles;

use Airlabs\Cms\Permission;
use Illuminate\Support\Facades\DB;

trait HasPermissions
{
    public function permissions()
    {
        return DB::table('airlabs_cms_permission_role')
            ->where([
                'role_id' => $this->id
            ])->pluck('permission');
    }

    public function givePermission(Permission $permission)
    {
        try {
            DB::table('airlabs_cms_permission_role')
                ->insert([
                    'permission' => $permission->name(),
                    'role_id' => $this->id
                ]);
        } catch (\Exception $e) {
        }
    }

    public function hasPermission(Permission $permission)
    {
        return DB::table('airlabs_cms_permission_role')
                ->where([
                    'permission' => $permission->name(),
                    'role_id' => $this->id
                ])->count() > 0;
    }
}
