<?php

namespace Airlabs\Cms\Roles;

use Airlabs\Cms\Role;

trait HasRoles
{

    public function role()
    {
        return $this->belongsTo(Role::class)
            ->withDefault([
                'name' => 'Guest'
            ]);
    }

    public function hasRole(Role $role)
    {
        return $this->role_id == $role->id;
    }

    public function giveRole(Role $role)
    {
        return $this->update([
            'role_id' => $role->id
        ]);
    }
}
