<?php

namespace Airlabs\Cms;

interface Plugin
{
    public function name(): string;

    public function label(): string;

    public function icon(): string;

    public function menu(): array;

    public function permissions(): array;
}
