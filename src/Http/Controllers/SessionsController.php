<?php

namespace Airlabs\Cms\Http\Controllers;

use Airlabs\Cms\Http\Requests\Sessions\CreateSessionRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    public function create()
    {
        return view('air::sessions.create');
    }

    public function store(CreateSessionRequest $request)
    {
        if(Auth::guard('air')->attempt(
            $request->only([ 'email', 'password'])
        )) {
            return redirect(air()->dashboardUrl());
        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);
    }

    public function destroy()
    {
        Auth::guard('air')->logout();

        request()->session()->invalidate();

        return redirect(air()->dashboardUrl());
    }
}
