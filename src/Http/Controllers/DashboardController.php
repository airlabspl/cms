<?php

namespace Airlabs\Cms\Http\Controllers;

class DashboardController extends Controller
{
    public function index()
    {
        $plugins = air()->plugins()->all();

        return view('air::dashboard', compact('plugins'));
    }
}
