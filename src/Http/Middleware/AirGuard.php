<?php

namespace Airlabs\Cms\Http\Middleware;

use Illuminate\Support\Facades\Auth;

class AirGuard
{
    public function handle($request, \Closure $next)
    {
        Auth::shouldUse('air');

        air()->permissions()->register();

        return $next($request);
    }
}
