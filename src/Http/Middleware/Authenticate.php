<?php

namespace Airlabs\Cms\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    public function handle($request, Closure $next, $guard = null)
    {
        if ( ! Auth::guard($guard)->check()) {
            if ($request->wantsJson()) {
                throw new AuthenticationException();
            }

            return redirect()->to(route('airlabs.cms.sessions.create'));
        }

        return $next($request);
    }
}
