<?php

use Airlabs\Cms\Air;

function air(): Air
{
    return resolve('air');
}
