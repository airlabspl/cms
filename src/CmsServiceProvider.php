<?php

namespace Airlabs\Cms;

use Airlabs\Cms\Console\Commands\CreateAdminCommand;
use Airlabs\Cms\Http\Middleware\AirGuard;
use Airlabs\Cms\Http\Middleware\Authenticate;
use Airlabs\Cms\Managers\PermissionsManager;
use Airlabs\Cms\Managers\PluginsManager;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->routes();
        $this->database();
        $this->guard();
        $this->views();
        $this->assets();
        $this->middleware();
        $this->console();
    }

    public function register()
    {
        $this->app->singleton('air', function ($app) {
            return new Air();
        });

        $this->app->singleton('air.permissions', function ($app) {
            return new PermissionsManager();
        });

        $this->app->singleton('air.plugins', function ($app) {
            return new PluginsManager();
        });
    }

    protected function routes()
    {
        Route::middleware('air')
            ->namespace('Airlabs\Cms\Http\Controllers')
            ->group(__DIR__ . '/../routes/cms.php');
    }

    protected function database()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->app->make('Illuminate\Database\Eloquent\Factory')
            ->load(__DIR__ . '/../database/factories');
    }

    protected function guard()
    {
        config()->set('auth.providers.air', [
            'driver' => 'eloquent',
            'model' => \Airlabs\Cms\User::class,
        ]);

        config()->set('auth.guards.air', [
            'driver' => 'session',
            'provider' => 'air',
        ]);
    }

    protected function views()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'air');
    }

    protected function assets()
    {
        $this->publishes([
            __DIR__.'/../public/' => public_path('vendor/airlabs/cms/')
        ], 'air');
    }

    protected function middleware()
    {
        resolve('router')->aliasMiddleware('airlabs.cms.auth', Authenticate::class);
        resolve('router')->aliasMiddleware('airlabs.cms.guard', AirGuard::class);

        resolve('router')->middlewareGroup('air', [
            'web', 'airlabs.cms.guard'
        ]);
    }

    protected function console()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateAdminCommand::class
            ]);
        }
    }
}
