<?php

namespace Airlabs\Cms\Tests\Fakes\Permissions;

use Airlabs\Cms\Permission;

class FakePermission implements Permission
{
    protected $name;

    public function __construct($name = 'fake-permission')
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function label(): string
    {
        return 'Fake Permission';
    }
}
