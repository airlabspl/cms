<?php

namespace Airlabs\Cms\Tests\Fakes\Policies;

use Airlabs\Cms\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FakePolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->can('update-role');
    }
}
