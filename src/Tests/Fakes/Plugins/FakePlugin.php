<?php

namespace Airlabs\Cms\Tests\Fakes\Plugins;

use Airlabs\Cms\Plugin;

class FakePlugin implements Plugin
{
    public function name(): string
    {
        return 'fake-plugin';
    }

    public function label(): string
    {
        return 'Fake Plugin';
    }

    public function icon(): string
    {
        return '';
    }

    public function menu(): array
    {
        return [];
    }

    public function permissions(): array
    {
        return [];
    }
}
