<?php

namespace Airlabs\Cms;

use Airlabs\Cms\Roles\HasPermissions;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasPermissions;

    protected $table = 'airlabs_cms_roles';

    protected $fillable = [ 'name' ];
}
