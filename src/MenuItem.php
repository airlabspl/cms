<?php

namespace Airlabs\Cms;

interface MenuItem
{
    public function label(): string;

    public function path(): string;
}
