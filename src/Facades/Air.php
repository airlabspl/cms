<?php

namespace Airlabs\Cms\Facades;

use Illuminate\Support\Facades\Facade;

class Air extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'air';
    }
}
