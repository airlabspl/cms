<?php

namespace Airlabs\Cms;

use Airlabs\Cms\Managers\PermissionsManager;
use Airlabs\Cms\Managers\PluginsManager;

class Air
{
    public function permissions(): PermissionsManager
    {
        return resolve('air.permissions');
    }

    public function plugins(): PluginsManager
    {
        return resolve('air.plugins');
    }

    public function dashboardUrl(): string
    {
        return config('air.dashboard.url', 'admin');
    }
}
