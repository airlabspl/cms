<?php

namespace Airlabs\Cms\Console\Commands;

use Airlabs\Cms\User;
use Illuminate\Console\Command;

class CreateAdminCommand extends Command
{
    protected $signature = 'create:admin';

    protected $description = 'Create Admin Account for AIRLabs CMS';

    public function handle()
    {
        $this->line("Creating an administrator account.");

        $name = $this->ask("What is the user name?");
        $email = $this->ask("What is the user email?");
        $password = $this->secret("What is the user password?");

        $attributes = [
          'name' => $name,
          'email' => $email,
          'password' => bcrypt($password),
          'is_admin' => true
        ];

        if (User::forceCreate($attributes)) {
            $this->info("Admin account created. You may now log in.");
            return;
        }

        $this->error("Account could not be created.");
    }
}
