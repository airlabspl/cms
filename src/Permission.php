<?php

namespace Airlabs\Cms;

interface Permission
{
    public function name(): string;

    public function label(): string;
}
