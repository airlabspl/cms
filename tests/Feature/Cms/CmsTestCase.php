<?php

namespace Tests\Feature\Cms;

use Airlabs\Cms\Permission;
use Airlabs\Cms\Role;
use Airlabs\Cms\User;
use Tests\TestCase;

class CmsTestCase extends TestCase
{
    protected function authorizeUserWithPermission(Permission $permission)
    {
        $role = factory(Role::class)->create();
        $role->givePermission($permission);

        $user = factory(User::class)->create();
        $user->giveRole($role);

        return $this->authorize($user);
    }

    protected function authorizeMember()
    {
        $user = factory(User::class)->create();

        return $this->authorize($user);
    }

    protected function authorizeAdmin()
    {
        $user = factory(User::class)->states('admin')->create();

        return $this->authorize($user);
    }

    protected function authorize($user)
    {
        $this->be($user, 'air');

        return $user;
    }
}
