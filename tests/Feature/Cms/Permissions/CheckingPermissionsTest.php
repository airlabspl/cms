<?php

namespace Tests\Feature\Cms\Permissions;

use Airlabs\Cms\Role;
use Airlabs\Cms\Tests\Fakes\Permissions\FakePermission;
use Airlabs\Cms\Tests\Fakes\Policies\FakePolicy;
use Airlabs\Cms\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Tests\Feature\Cms\CmsTestCase;
use Tests\TestCase;

class CheckingPermissionsTest extends CmsTestCase
{
    use RefreshDatabase;

    /** @test */
    function unprotected_route_can_be_accessed()
    {
        Route::get($uri = str_random(), function () { return 'Test'; })
            ->middleware('air');

        $this->getJson($uri)
            ->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    function protected_route_cannot_be_accessed_without_permissions()
    {
        $user = factory(User::class)->create();

        Route::get($uri = str_random(), function () { return 'Test'; })
            ->middleware('can:do-something');

        $this->be($user->fresh(), 'air');

        $this->getJson($uri)
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    function protected_route_can_be_accessed_with_permissions()
    {
        $this->authorizeUserWithPermission($permission = new FakePermission());

        Route::get($uri = str_random(), function () { return 'Test'; })
            ->middleware([ 'air', 'can:' . $permission->name() ]);

        $this->getJson($uri)
            ->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    function permission_key_has_to_be_the_one_specified()
    {
        $this->authorizeUserWithPermission(new FakePermission());

        Route::get($uri = str_random(), function () { return 'Test'; })
            ->middleware('can:do-something-else');

        $this->getJson($uri)
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
