window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

window.Vue = require('vue');
window.bus = new Vue({});

Vue.component('confirm', require('./components/Confirm.vue'));
Vue.component('dashboard', require('./components/Dashboard.vue'));
Vue.component('modal', require('./components/Modal.vue'));
Vue.component('spinner', require('./components/Spinner.vue'));

/** Users */
Vue.component('edit-user', require('./components/Users/EditUser.vue'));
Vue.component('create-user', require('./components/Users/CreateUser.vue'));
Vue.component('remove-user', require('./components/Users/RemoveUser.vue'));

window.confirm = (data) => {
    window.bus.$emit('confirm-show', data);
};

const app = new Vue({
    el: '#app'
});
