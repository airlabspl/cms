@extends('air::layouts.cms')

@section('content')
    @include('air::breadcrumb', [ 'module' => 'Log in' ])

    <div class="mw8 center pa3 pa4-ns">
        <h1 class="black-70 f3 fw5 mb5">
            Logowanie
        </h1>

        @if($errors->any())
        <div class="mb4 lh-copy ba br2 b--dark-red bg-washed-red dark-red">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="{{ route('airlabs.cms.sessions.store') }}" method="POST">
            {{ csrf_field() }}

            <div class="mb4">
                <label for="email" class="Form__Label">
                    Adres e-mail
                </label>
                <input type="email"
                       id="email"
                       value="{{ old('email') }}"
                       name="email"
                       autofocus="true"
                       class="Form__Input">
            </div>

            <div class="mb4">
                <label for="password" class="Form__Label">
                    Hasło
                </label>
                <input type="password"
                       id="password"
                       name="password"
                       class="Form__Input">
            </div>

            <div class="mb4">
                <button class="Form__Submit">
                    Zaloguj się
                </button>
            </div>
        </form>
    </div>

@endsection