<article class="w-100 w-third-l ph3">
    <div class="cf mt4 mb0-l pv5 tc scale ba b--black-10 bg-white">
        <a href="{{ url($plugin->name()) }}">
            <img src="{{ asset($plugin->icon()) }}" alt="" class="mb3 is-96x96">
        </a>
        <div class="">
            <a href="{{ url($plugin->name()) }}" class="link">
                <h2 class="f4 fw5 black-70 mt0 mb2">
                    {{ $plugin->label() }}
                </h2>
            </a>
        </div>
    </div>
</article>