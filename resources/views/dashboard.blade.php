@extends('air::layouts.cms')

@section('content')

    @include('air::breadcrumb')

    <dashboard inline-template>
        <div class="ph4 ph2-l pv2 mw8 center cf mt0 mt3-ns flex flex-wrap">
            @foreach($plugins as $plugin)
                @include('air::plugin')
            @endforeach
        </div>
    </dashboard>
@endsection