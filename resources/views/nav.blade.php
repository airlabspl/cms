<div class="Stripe"></div>

<div class="bg-white bb b--black-10 pv4">
    <div class="ph3 ph3 mw8 center">
        <div class="cf">
            <div class="fl">
                <a href="{{ url(air()->dashboardUrl()) }}">
                    <img src="{{ asset('vendor/airlabs/cms/img/dashboard-mono.png') }}" alt="" class="v-mid">
                </a>

                @if(auth('air')->check())
                <a href="{{ url('/') }}" class="link">
                    <h2 class="f5 fw6 black-70 dim dn dib-ns v-mid ml2">
                        Podgląd
                    </h2>
                </a>
                @endif
            </div>
            <div class="fr">
                @if(auth('air')->check())
                <h2 class="f5 fw6 black-70 dim dn dib-ns v-mid">
                    {{ auth('air')->user()->name }}
                </h2>
                <img src="{{ asset('vendor/airlabs/cms/img/avatars/'. auth('air')->user()->avatar .'.png') }}" alt=""
                     class="dib v-mid ml2 br-pill bw1 b--black-70 is-48x48">
                @else
                    <a href="{{ url('/') }}" class="link">
                        <h2 class="f4 fw5 black-70 dim dn dib-ns v-mid">
                            Podgląd
                        </h2>
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>