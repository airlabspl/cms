
<span v-if="form.errors.has('{{ $name }}')"
      v-text="form.errors.first('{{ $name }}')"
      class="dark-red f6 pa2 dib i"></span>