<div class="bg-white-50 bb pa2 b--black-10">
    <div class="mw8 center pa3 black-50 cf">
        <div class="relative dib dn-ns ml3 v-mid hide-child">
            <span class="link black-70 fw6">
                Menu
            </span>
             <div class="child absolute top-1 pv3 ph2 bg-white ba b--black-10 tl left-0 br2 mt2 overflow-x-hidden z-999"
                  style="min-width: 200px; max-width: 250px;">
                 <a href="{{ url(air()->dashboardUrl()) }}"
                    class="link black-70 fw6 db pa2">
                     Panel Zarządzania
                 </a>
                 @if(isset($plugin))
                     <a href="{{ url($plugin->name()) }}"
                        class="link black-70 fw6 db pa2">
                         {{ $plugin->label() }}
                     </a>
                 @endif

                 @if(isset($menu))
                     <a href="{{ url($menu->path()) }}"
                        class="link black-70 fw6 db pa2">
                         {{ $menu->label() }}
                     </a>
                 @endif
             </div>
        </div>


        <div class="fl dn dib-ns">
            <a href="{{ url(air()->dashboardUrl()) }}"
               class="link black-70 fw6">
                Panel Zarządzania
            </a>

            @if(isset($plugin))
                <span class="ph2">/</span>
                <a href="{{ url($plugin->name()) }}"
                   class="link black-70 fw6">
                    {{ $plugin->label() }}
                </a>
            @endif

            @if(isset($menu))
                <span class="ph2">/</span>
                <a href="{{ url($menu->path()) }}"
                   class="link black-70 fw6">
                    {{ $menu->label() }}
                </a>
            @endif

            @if(isset($current))
                <span class="ph2">/</span>
                <span class="link black-70 fw4">
                    {{ $current }}
                </span>
            @endif
        </div>
        <div class="fr">
            @if(auth('air')->check())
            <a href="{{ route('airlabs.cms.sessions.destroy') }}"
               class="link black-70 fw6">
                Wyloguj
            </a>
            @endif
        </div>
    </div>
</div>