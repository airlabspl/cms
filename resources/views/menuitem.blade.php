<a href="{{ $menu->path() }}" class="link">
    <div class="f4 black-20 fw5 pointer pv1 lh-copy dim">
        - {{ $menu->label() }}
    </div>
</a>