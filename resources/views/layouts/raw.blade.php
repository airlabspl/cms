<!DOCTYPE html>
<html lang="pl" class="min-h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/vendor/airlabs/cms/css/cms.css?v={{ str_random() }}">
    <title>Panel Zarządzania @yield('title')</title>
    @yield('styles')
</head>
<body class="min-h-100">
<div id="app">
    @yield('app')

    <confirm></confirm>
</div>
<script src="/vendor/airlabs/cms/js/cms.js?v={{ str_random() }}"></script>
@yield('scripts')
</body>
</html>