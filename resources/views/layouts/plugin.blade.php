@extends('air::layouts.cms')

@section('content')
    @include('air::breadcrumb')

    <div class="Section__Container flex-l">
        @if(isset($plugin))
        <div class="mr4-l w-25-l mb4">
            @foreach ($plugin->menu() as $menuItem)
                <a href="{{ url($menuItem->path()) }}"
                   class="link black-70 fw6 db pv2 dim mb2 lh-copy">
                    {{ $menuItem->label() }}
                </a>
            @endforeach
        </div>
        @endif

        <div class="flex-grow">
            @yield('plugin')
        </div>
    </div>
@endsection