@extends('air::layouts.raw')

@section('app')
    @include('air::nav')

    @yield('content')
@endsection